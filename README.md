# remove-from-array

[![build status](https://gitlab.com/ci/projects/107935/status.png?ref=master)](https://gitlab.com/ci/projects/107935?ref=master)

remove an index from an array

```
npm install @amphibian/remove-from-array
```

```javascript
var removeFromArray = require('@amphibian/remove-from-array');
var mammals = ['cat', 'bird', 'dog', 'horse', 'fish'];

removeFromArray('fish', mammals);
removeFromArray('bird', mammals);

console.log(mammals); // > ['cat', 'dog', 'horse']
```
